# Shopper

Create shopping lists with items and assemblies (groups of items)

## Running locally

### Verify Ruby and Node

* `ruby` - 2.6.3
* `node` - 11.10.1

### Installation

```bash
$ bundle install
$ rails db:create
$ rails db:migrate
$ rails db:seed
```
